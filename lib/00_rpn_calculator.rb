class RPNCalculator

  def initialize
    @stack = []
  end

  def push(number)
    @stack << number
  end

  def plus
    raise "calculator is empty" if @stack.length < 2
    @stack.length > 1 ? pop = @stack.pop(2).reduce(:+) : pop = 2
    @stack << pop
  end

  def minus
    raise "calculator is empty" if @stack.length < 2
    pop = @stack.pop(2).reduce(:-)
    @stack << pop
  end

  def times
    raise "calculator is empty" if @stack.length < 2
    pop = @stack.pop(2).reduce(:*)
    @stack << pop
  end

  def divide
    raise "calculator is empty" if @stack.length < 2
    pop = @stack.pop(2)
    pop[0] = pop[0].to_f
    pop[1] = pop[1].to_f
    pop = pop.reduce(:/)
    @stack << pop
  end

  def value
    @stack[-1]
  end

  def tokens(string)
    tkns = string.split
    tkns.map do |el|
      ('1'..'9').include?(el) ? el.to_i : el.to_sym
    end
  end

  def evaluate(nums)
    calculation = tokens(nums)
    while calculation.length > 0
      n = calculation.shift
      if n.is_a? Integer
        self.push(n)
      elsif n == :+
        self.plus
      elsif n == :-
        self.minus
      elsif n == :*
        self.times
      elsif n == :/
        self.divide
      end
    end
    self.value
  end
end
